﻿-- Update from ShadowHack3 wow server, autor Vitasic.


-- (C) ShadowHack3
UPDATE `creature_template` SET `subname` = 'ShadowHack3' WHERE `subname` = 'ForsakeN WoW';
UPDATE `creature_template` SET `subname` = 'ShadowHack3' WHERE `subname` = ' Forsaken - WoW';
-- UPDATE `creature_template` SET `IconName` = 'ShadowHack3' WHERE `IconName` = ' Forsaken WoW';
UPDATE `locales_creature` SET `subname_loc8` = 'ShadowHack3' WHERE `subname_loc8` = 'ForsakeN-WoW';
UPDATE `item_template` SET `description` = 'ShadowHack3' WHERE `description` = 'ForsakeN - WoW';
UPDATE `item_template` SET `description` = 'ShadowHack3' WHERE `description` = ' ForsakeN - WoW';

-- Gameobject Cooldown Reset
INSERT INTO gameobject_template VALUES
(42001, 3, 2616, 'Cooldown Reset', '', '', '', 0, 0, 0.2, 0, 0, 0, 0, 0, 0, 0, -1, 600, 0, 0, 0, -1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 'go_cooldown_reset', 12340);

-- Запрос к моду "Возможность писать в чат, исходя из проведенного онлайна"
DELETE FROM `trinity_string` WHERE `entry` IN (11007,11008,11009,11010);
INSERT INTO `trinity_string` (`entry`, `content_default`, `content_loc8`) VALUES
('11007', 'Your chat is disabled. So you can write to chat, draw in the game for another %s seconds', 'Ваш чат отключен. Чтобы вы смогли написать в чат, проведите в игре еще %s секунд'),
('11008', 'You cannot say, yell or emote until you will play %d minutes.', 'Вы не сможете говорить, кричать, использовать эмоции, пока вы не проведете в онлайне %d минут.'),
('11009', 'You cannot whisper until you will play %d minutes.', 'Вы не сможете шептать, пока вы не проведете в онлайне %d минут.'),
('11010', 'You cannot write to channels until you will play %d. minutes.', 'Вы не сможете писать в каналы, пока вы не проведете в онлайне %d минут.');
 
-- Disanled crash spell's
DELETE FROM `disables` WHERE `sourceType`=0 AND `entry` IN (23789,61904,40851,61905,39090);
INSERT INTO `disables` (`sourceType`, `entry`, `flags`, `comment`) VALUES
(0, 23789, 1, 'Stoneclaw Totem TEST - can crash client by spawning too many totems'),
(0, 61904, 1, 'Magma Totem TEST - can crash client by spawning too many totems'),
(0, 40851, 1, 'Disable spell which players can use with Mind Control'),
(0, 61905, 1, 'Magma Totem Test Effect'),
(0, 39090, 1, 'Positive Charge');

-- Update NPC
UPDATE `creature_template` SET `modelid1` = '20748', `modelid2` = '0', `modelid3` = '0', `modelid4` = '0' WHERE `entry` = 200086;
UPDATE `creature_template` SET `modelid1` = '20662', `modelid2` = '0', `modelid3` = '0', `modelid4` = '0' WHERE `entry` = 200087;


-- absorbproc.sql
REPLACE INTO spell_proc_event (`entry`, `SchoolMask`, `SpellFamilyName`, `SpellFamilyMask0`, `SpellFamilyMask1`, `SpellFamilyMask2`, `procFlags`, `procEx`, `ppmRate`, `CustomChance`, `Cooldown`) VALUES
(11119, 4, 3, 0, 0, 0, 0, 262146, 0, 0, 0),
(11120, 4, 3, 0, 0, 0, 0, 262146, 0, 0, 0),
(12846, 4, 3, 0, 0, 0, 0, 262146, 0, 0, 0),
(12847, 4, 3, 0, 0, 0, 0, 262146, 0, 0, 0),
(12848, 4, 3, 0, 0, 0, 0, 262146, 0, 0, 0),
(30296, 0, 5, 897, 8519872, 0, 0, 262144, 0, 0, 0),
(30295, 0, 5, 897, 8519872, 0, 0, 262144, 0, 0, 0),
(30293, 0, 5, 897, 8519872, 0, 0, 262144, 0, 0, 0),
(30823, 0, 0, 0, 0, 0, 0, 262144, 10, 0, 0),
( 31876, 0x00,  10, 0x00800000, 0x00000000, 0x00000000, 0x00000000, 0x00040000,   0,   0,   0), -- Judgements of the Wise (Rank 1)
( 31877, 0x00,  10, 0x00800000, 0x00000000, 0x00000000, 0x00000000, 0x00040000,   0,   0,   0), -- Judgements of the Wise (Rank 2)
( 31878, 0x00,  10, 0x00800000, 0x00000000, 0x00000000, 0x00000000, 0x00040000,   0,   0,   0), -- Judgements of the Wise (Rank 3)
(34258, 0, 10, 8388608, 0, 0, 0, 262144, 0, 0, 0),
(34262, 0, 10, 8388608, 0, 0, 0, 262144, 0, 0, 0),
(37195, 0, 10, 8388608, 0, 0, 0, 262144, 0, 0, 0),
(40470, 0, 10, 3229614080, 0, 0, 0, 262144, 0, 0, 0),
(49194, 0, 15, 8192, 0, 0, 0, 262144, 0, 0, 0),
(48835, 0, 10, 8388608, 0, 0, 0, 262144, 0, 0, 0),
(51521, 0, 11, 0, 16777216, 0, 0, 1, 0, 0, 0),
(51522, 0, 11, 0, 16777216, 0, 0, 1, 0, 0, 0),
(51528, 0, 0, 0, 0, 0, 0, 262144, 2.5, 0, 0),
(51529, 0, 0, 0, 0, 0, 0, 262144, 5, 0, 0),
(51530, 0, 0, 0, 0, 0, 0, 262144, 7.5, 0, 0),
(51531, 0, 0, 0, 0, 0, 0, 262144, 10, 0, 0),
(51532, 0, 0, 0, 0, 0, 0, 262144, 12, 0, 0),
(53380, 0, 10, 8388608, 163840, 0, 0, 262146, 0, 0, 0),
(53381, 0, 10, 8388608, 163840, 0, 0, 262146, 0, 0, 0),
(53382, 0, 10, 8388608, 163840, 0, 0, 262146, 0, 0, 0),
(53671, 0, 10, 8388608, 0, 0, 0, 262144, 0, 0, 0),
(53673, 0, 10, 8388608, 0, 0, 0, 262144, 0, 0, 0),
(54151, 0, 10, 8388608, 0, 0, 0, 262144, 0, 0, 0),
(54154, 0, 10, 8388608, 0, 0, 0, 262144, 0, 0, 0),
(54155, 0, 10, 8388608, 0, 0, 0, 262144, 0, 0, 0),
(63108, 0, 5, 2, 0, 0, 0, 262144, 0, 0, 0),
(49217, 0, 15, 0, 0, 2, 0, 262144, 0, 0, 1),
(49654, 0, 15, 0, 0, 2, 0, 262144, 0, 0, 1),
(49655, 0, 15, 0, 0, 2, 0, 262144, 0, 0, 1),
(56636, 0, 4, 32, 0, 0, 0, 262144, 0, 0, 6),
(56637, 0, 4, 32, 0, 0, 0, 262144, 0, 0, 6),
(56638, 0, 4, 32, 0, 0, 0, 262144, 0, 0, 6),
(58877, 0, 0, 0, 0, 0, 0, 262144, 0, 0, 0);


-- challenge_faststart.sql
-- World db
DELETE FROM `gameobject_template` WHERE `entry` = 42000;
INSERT INTO `gameobject_template`(`entry`, `type`, `displayId`, `name`, `IconName`, `size`, `ScriptName`, `WDBVerified`) VALUES(42000, 10, 327, 'Faster Start', 'PVP' , 1, 'FastArenaCrystal', 12340);

DELETE FROM `command` WHERE `name` = 'challenge';
INSERT INTO `command` VALUES ('challenge', '0', 'Syntax: .challenge [$name]\nInvite you and your target to 1x1 arena.');
DELETE FROM `command` WHERE `name` = 'challenge players';
INSERT INTO `command` VALUES ('challenge players', '1', 'Syntax: .challenge players $name1 $name2\nInvite two players to 1x1 arena.');


-- fake_items.sql
DELETE FROM creature_template WHERE entry = '190001'; 
INSERT INTO creature_template (entry, modelid1, name, subname, IconName, gossip_menu_id, minlevel, maxlevel, Health_mod, Mana_mod, Armor_mod, faction_A, faction_H, npcflag, speed_walk, speed_run, scale, rank, dmg_multiplier, unit_class, unit_flags, type, type_flags, InhabitType, RegenHealth, flags_extra, ScriptName) VALUES 
('190001', '15998', "Transmogrify Master", "", 'Speak', '50000', 71, 71, 1.56, 1.56, 1.56, 35, 35, 3, 1, 1.14286, 1.25, 1, 1, 1, 2, 7, 138936390, 3, 1, 2, 'npc_transmogrify'); 

INSERT INTO `gossip_menu` VALUES (51000, 51000);
INSERT INTO npc_text (ID, text0_0, em0_1) VALUES
(51000, 'Put in the first slot of bag item, that you want to transmogrify. In the second slot, put item with perfect display.', 0);


--DELETE FROM creature_template WHERE entry = '190017'; 
--INSERT INTO creature_template (entry, modelid1, name, subname, IconName, gossip_menu_id, minlevel, maxlevel, Health_mod, Mana_mod, Armor_mod, faction_A, faction_H, npcflag, speed_walk, speed_run, scale, rank, dmg_multiplier, unit_class, unit_flags, type, type_flags, InhabitType, RegenHealth, flags_extra, ScriptName) VALUES 
--('190017', '25618', "Надзиратели арены", "", 'Speak', '0', 71, 71, 1.56, 1.56, 1.56, 35, 35, 3, 1, 1.14286, 1.25, 1, 1, 1, 2, 7, 138936390, 3, 1, 2, 'spectator'); 


-- OLD UPDATE
UPDATE `creature_template` SET `spell1` = 6474, `spell2` = 0 WHERE `entry` = 2630;

-- Anti-Magic Zone
REPLACE INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction_A`, `faction_H`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `equipment_id`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `WDBVerified`) VALUES (28306, 0, 0, 0, 0, 0, 20242, 0, 0, 0, 'Anti-Magic Zone', '', '', 0, 80, 80, 0, 2, 2, 0, 1, 1.14286, 1, 0, 420, 630, 0, 158, 1.1, 2000, 2000, 1, 33554434, 0, 0, 0, 0, 0, 0, 336, 504, 126, 11, 1024, 0, 0, 0, 0, 0, 0, 0, 0, 0, 50461, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', 0, 3, 2.70535, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, '', 11403);

-- (12577) Arcane Concentration (Rank 5)
DELETE FROM `spell_proc_event` WHERE `entry` IN (12577);
INSERT INTO `spell_proc_event` VALUES (12577, 0x00, 0x03, 0x00000000, 0x00000000, 0x00000000, 0x00010000, 0x00000001, 0, 10, 0);

-- (12576) Arcane Concentration (Rank 4)
DELETE FROM `spell_proc_event` WHERE `entry` IN (12576);
INSERT INTO `spell_proc_event` VALUES (12576, 0x00, 0x03, 0x00000000, 0x00000000, 0x00000000, 0x00010000, 0x00000001, 0, 8, 0);

-- (12575) Arcane Concentration (Rank 3)
DELETE FROM `spell_proc_event` WHERE `entry` IN (12575);
INSERT INTO `spell_proc_event` VALUES (12575, 0x00, 0x03, 0x00000000, 0x00000000, 0x00000000, 0x00010000, 0x00000001, 0, 6, 0);

-- (12574) Arcane Concentration (Rank 2)
DELETE FROM `spell_proc_event` WHERE `entry` IN (12574);
INSERT INTO `spell_proc_event` VALUES (12574, 0x00, 0x03, 0x00000000, 0x00000000, 0x00000000, 0x00010000, 0x00000001, 0, 4, 0);

-- (12573) Arcane Concentration (Rank 1)
DELETE FROM `spell_proc_event` WHERE `entry` IN (12573);
INSERT INTO `spell_proc_event` VALUES (12573, 0x00, 0x03, 0x00000000, 0x00000000, 0x00000000, 0x00010000, 0x00000001, 0, 2, 0);

-- scriptname for Nibelung
UPDATE `creature_template` SET `ScriptName`='npc_valkyr' WHERE (`entry`='38392');

-- item requirements custom patch
ALTER TABLE `item_template`
DROP COLUMN `userating`;

ALTER TABLE `item_requirements`
ADD COLUMN `ratinbracket` tinyint(1) unsigned NOT NULL DEFAULT '0' AFTER `reqrating`;

CREATE TABLE IF NOT EXISTS `item_requirements` (
  `entry` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0,
  `reqrating` MEDIUMINT(8) UNSIGNED NOT NULL DEFAULT 0,
  `ratinbracket` TINYINT(1) UNSIGNED NOT NULL DEFAULT 0,
  `reqitem` MEDIUMINT(8) NOT NULL DEFAULT 0,
  PRIMARY KEY (`entry`)
) ENGINE=MYISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;
	
UPDATE `spell_bonus_data` SET `direct_bonus`='0' WHERE (`entry`='45297');
UPDATE `spell_bonus_data` SET `direct_bonus`='0' WHERE (`entry`='45284');

-- shadowmeld combat
INSERT INTO `spell_linked_spell` VALUES ('58984', '54661', '0', 'Shadowmeld - Sanctuary');

REPLACE INTO `spell_bonus_data` VALUES ('9007', '0', '0', '0', '0.03', 'Pounce - Bleed');

-- REPLACE INTO `spell_dbc` (`Id`, `Dispel`, `Mechanic`, `Attributes`, `AttributesEx`, `AttributesEx2`, `AttributesEx3`, `AttributesEx4`, `AttributesEx5`, `Stances`, `StancesNot`, `Targets`, `CastingTimeIndex`, `AuraInterruptFlags`, `ProcFlags`, `ProcChance`, `ProcCharges`, `MaxLevel`, `BaseLevel`, `SpellLevel`, `DurationIndex`, `RangeIndex`, `StackAmount`, `EquippedItemClass`, `EquippedItemSubClassMask`, `EquippedItemInventoryTypeMask`, `Effect1`, `Effect2`, `Effect3`, `EffectDieSides1`, `EffectDieSides2`, `EffectDieSides3`, `EffectRealPointsPerLevel1`, `EffectRealPointsPerLevel2`, `EffectRealPointsPerLevel3`, `EffectBasePoints1`, `EffectBasePoints2`, `EffectBasePoints3`, `EffectMechanic1`, `EffectMechanic2`, `EffectMechanic3`, `EffectImplicitTargetA1`, `EffectImplicitTargetA2`, `EffectImplicitTargetA3`, `EffectImplicitTargetB1`, `EffectImplicitTargetB2`, `EffectImplicitTargetB3`, `EffectRadiusIndex1`, `EffectRadiusIndex2`, `EffectRadiusIndex3`, `EffectApplyAuraName1`, `EffectApplyAuraName2`, `EffectApplyAuraName3`, `EffectAmplitude1`, `EffectAmplitude2`, `EffectAmplitude3`, `EffectMultipleValue1`, `EffectMultipleValue2`, `EffectMultipleValue3`, `EffectMiscValue1`, `EffectMiscValue2`, `EffectMiscValue3`, `EffectMiscValueB1`, `EffectMiscValueB2`, `EffectMiscValueB3`, `EffectTriggerSpell1`, `EffectTriggerSpell2`, `EffectTriggerSpell3`, `EffectSpellClassMaskA1`, `EffectSpellClassMaskA2`, `EffectSpellClassMaskA3`, `EffectSpellClassMaskB1`, `EffectSpellClassMaskB2`, `EffectSpellClassMaskB3`, `EffectSpellClassMaskC1`, `EffectSpellClassMaskC2`, `EffectSpellClassMaskC3`, `MaxTargetLevel`, `SpellFamilyName`, `SpellFamilyFlags1`, `SpellFamilyFlags2`, `SpellFamilyFlags3`, `MaxAffectedTargets`, `DmgClass`, `PreventionType`, `DmgMultiplier1`, `DmgMultiplier2`, `DmgMultiplier3`, `AreaGroupId`, `SchoolMask`, `Comment`) VALUES ('200002', '0', '0', '0', '32', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '80', '80', '327', '1', '0', '-1', '0', '0', '6', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '40', '0', '0', '0', '0', '0', '4', '0', '0', '127', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Vanish - Immunity');

-- Vanish - immunity
-- REPLACE INTO `spell_linked_spell` VALUES ('1857', '200002', '0', 'Vanish - Immunity');
-- REPLACE INTO `spell_linked_spell` VALUES ('26889', '200002', '0', 'Vanish - Immunity');
-- REPLACE INTO `spell_linked_spell` VALUES ('1856', '200002', '0', 'Vanish - Immunity');

REPLACE INTO `spell_dbc` VALUES ('200005', '0', '0', '0', '32', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '407', '1', '0', '-1', '0', '0', '6', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '4', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Vanish - Hide time');

UPDATE `trinity_string` SET `content_default`='id: %d eff: %d amount: %d, caster - %s, itemcaster - %s' WHERE (`entry`='470');
REPLACE INTO `spell_dbc` (`Id`, `Dispel`, `Mechanic`, `Attributes`, `AttributesEx`, `AttributesEx2`, `AttributesEx3`, `AttributesEx4`, `AttributesEx5`, `Stances`, `StancesNot`, `Targets`, `CastingTimeIndex`, `AuraInterruptFlags`, `ProcFlags`, `ProcChance`, `ProcCharges`, `MaxLevel`, `BaseLevel`, `SpellLevel`, `DurationIndex`, `RangeIndex`, `StackAmount`, `EquippedItemClass`, `EquippedItemSubClassMask`, `EquippedItemInventoryTypeMask`, `Effect1`, `Effect2`, `Effect3`, `EffectDieSides1`, `EffectDieSides2`, `EffectDieSides3`, `EffectRealPointsPerLevel1`, `EffectRealPointsPerLevel2`, `EffectRealPointsPerLevel3`, `EffectBasePoints1`, `EffectBasePoints2`, `EffectBasePoints3`, `EffectMechanic1`, `EffectMechanic2`, `EffectMechanic3`, `EffectImplicitTargetA1`, `EffectImplicitTargetA2`, `EffectImplicitTargetA3`, `EffectImplicitTargetB1`, `EffectImplicitTargetB2`, `EffectImplicitTargetB3`, `EffectRadiusIndex1`, `EffectRadiusIndex2`, `EffectRadiusIndex3`, `EffectApplyAuraName1`, `EffectApplyAuraName2`, `EffectApplyAuraName3`, `EffectAmplitude1`, `EffectAmplitude2`, `EffectAmplitude3`, `EffectMultipleValue1`, `EffectMultipleValue2`, `EffectMultipleValue3`, `EffectMiscValue1`, `EffectMiscValue2`, `EffectMiscValue3`, `EffectMiscValueB1`, `EffectMiscValueB2`, `EffectMiscValueB3`, `EffectTriggerSpell1`, `EffectTriggerSpell2`, `EffectTriggerSpell3`, `EffectSpellClassMaskA1`, `EffectSpellClassMaskA2`, `EffectSpellClassMaskA3`, `EffectSpellClassMaskB1`, `EffectSpellClassMaskB2`, `EffectSpellClassMaskB3`, `EffectSpellClassMaskC1`, `EffectSpellClassMaskC2`, `EffectSpellClassMaskC3`, `MaxTargetLevel`, `SpellFamilyName`, `SpellFamilyFlags1`, `SpellFamilyFlags2`, `SpellFamilyFlags3`, `MaxAffectedTargets`, `DmgClass`, `PreventionType`, `DmgMultiplier1`, `DmgMultiplier2`, `DmgMultiplier3`, `AreaGroupId`, `SchoolMask`, `Comment`) VALUES ('200006', '0', '0', '0', '1056', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '0', '0', '0', '13', '0', '-1', '0', '0', '3', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '25', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'Combat');

INSERT INTO `spell_dbc` (`Id`, `Dispel`, `Mechanic`, `Attributes`, `AttributesEx`, `AttributesEx2`, `AttributesEx3`, `AttributesEx4`, `AttributesEx5`, `Stances`, `StancesNot`, `Targets`, `CastingTimeIndex`, `AuraInterruptFlags`, `ProcFlags`, `ProcChance`, `ProcCharges`, `MaxLevel`, `BaseLevel`, `SpellLevel`, `DurationIndex`, `RangeIndex`, `StackAmount`, `EquippedItemClass`, `EquippedItemSubClassMask`, `EquippedItemInventoryTypeMask`, `Effect1`, `Effect2`, `Effect3`, `EffectDieSides1`, `EffectDieSides2`, `EffectDieSides3`, `EffectRealPointsPerLevel1`, `EffectRealPointsPerLevel2`, `EffectRealPointsPerLevel3`, `EffectBasePoints1`, `EffectBasePoints2`, `EffectBasePoints3`, `EffectMechanic1`, `EffectMechanic2`, `EffectMechanic3`, `EffectImplicitTargetA1`, `EffectImplicitTargetA2`, `EffectImplicitTargetA3`, `EffectImplicitTargetB1`, `EffectImplicitTargetB2`, `EffectImplicitTargetB3`, `EffectRadiusIndex1`, `EffectRadiusIndex2`, `EffectRadiusIndex3`, `EffectApplyAuraName1`, `EffectApplyAuraName2`, `EffectApplyAuraName3`, `EffectAmplitude1`, `EffectAmplitude2`, `EffectAmplitude3`, `EffectMultipleValue1`, `EffectMultipleValue2`, `EffectMultipleValue3`, `EffectMiscValue1`, `EffectMiscValue2`, `EffectMiscValue3`, `EffectMiscValueB1`, `EffectMiscValueB2`, `EffectMiscValueB3`, `EffectTriggerSpell1`, `EffectTriggerSpell2`, `EffectTriggerSpell3`, `EffectSpellClassMaskA1`, `EffectSpellClassMaskA2`, `EffectSpellClassMaskA3`, `EffectSpellClassMaskB1`, `EffectSpellClassMaskB2`, `EffectSpellClassMaskB3`, `EffectSpellClassMaskC1`, `EffectSpellClassMaskC2`, `EffectSpellClassMaskC3`, `MaxTargetLevel`, `SpellFamilyName`, `SpellFamilyFlags1`, `SpellFamilyFlags2`, `SpellFamilyFlags3`, `MaxAffectedTargets`, `DmgClass`, `PreventionType`, `DmgMultiplier1`, `DmgMultiplier2`, `DmgMultiplier3`, `AreaGroupId`, `SchoolMask`, `Comment`) VALUES ('110000', '0', '0', '464', '131072', '0', '67108864', '524288', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '80', '80', '21', '11', '0', '-1', '0', '0', '65', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '22', '0', '0', '30', '0', '0', '11', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'test spectator spell');
INSERT INTO `spell_dbc` (`Id`, `Dispel`, `Mechanic`, `Attributes`, `AttributesEx`, `AttributesEx2`, `AttributesEx3`, `AttributesEx4`, `AttributesEx5`, `Stances`, `StancesNot`, `Targets`, `CastingTimeIndex`, `AuraInterruptFlags`, `ProcFlags`, `ProcChance`, `ProcCharges`, `MaxLevel`, `BaseLevel`, `SpellLevel`, `DurationIndex`, `RangeIndex`, `StackAmount`, `EquippedItemClass`, `EquippedItemSubClassMask`, `EquippedItemInventoryTypeMask`, `Effect1`, `Effect2`, `Effect3`, `EffectDieSides1`, `EffectDieSides2`, `EffectDieSides3`, `EffectRealPointsPerLevel1`, `EffectRealPointsPerLevel2`, `EffectRealPointsPerLevel3`, `EffectBasePoints1`, `EffectBasePoints2`, `EffectBasePoints3`, `EffectMechanic1`, `EffectMechanic2`, `EffectMechanic3`, `EffectImplicitTargetA1`, `EffectImplicitTargetA2`, `EffectImplicitTargetA3`, `EffectImplicitTargetB1`, `EffectImplicitTargetB2`, `EffectImplicitTargetB3`, `EffectRadiusIndex1`, `EffectRadiusIndex2`, `EffectRadiusIndex3`, `EffectApplyAuraName1`, `EffectApplyAuraName2`, `EffectApplyAuraName3`, `EffectAmplitude1`, `EffectAmplitude2`, `EffectAmplitude3`, `EffectMultipleValue1`, `EffectMultipleValue2`, `EffectMultipleValue3`, `EffectMiscValue1`, `EffectMiscValue2`, `EffectMiscValue3`, `EffectMiscValueB1`, `EffectMiscValueB2`, `EffectMiscValueB3`, `EffectTriggerSpell1`, `EffectTriggerSpell2`, `EffectTriggerSpell3`, `EffectSpellClassMaskA1`, `EffectSpellClassMaskA2`, `EffectSpellClassMaskA3`, `EffectSpellClassMaskB1`, `EffectSpellClassMaskB2`, `EffectSpellClassMaskB3`, `EffectSpellClassMaskC1`, `EffectSpellClassMaskC2`, `EffectSpellClassMaskC3`, `MaxTargetLevel`, `SpellFamilyName`, `SpellFamilyFlags1`, `SpellFamilyFlags2`, `SpellFamilyFlags3`, `MaxAffectedTargets`, `DmgClass`, `PreventionType`, `DmgMultiplier1`, `DmgMultiplier2`, `DmgMultiplier3`, `AreaGroupId`, `SchoolMask`, `Comment`) VALUES ('115000', '0', '0', '464', '131072', '0', '67108864', '524288', '0', '0', '0', '0', '1', '0', '0', '0', '0', '0', '80', '80', '165', '11', '0', '-1', '0', '0', '65', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '1', '0', '0', '30', '0', '0', '11', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', 'test spectator spell');


############################################
# ONLI PVP SERVER
# DB CLEANUP
############################################
/*
-- Instance closed.
UPDATE `access_requirement` SET
`completed_achievement` = 9999,
`level_min` = 9999,
`level_max` = 9999,
`item` = 9999,
`item2` = 9999,
`quest_done_A` = 9999,
`quest_done_H` = 9999,
`quest_failed_text` = '<<< Instans Closed! >>>',
`completed_achievement` = 9999;

UPDATE creature_template SET ScriptName = "" WHERE difficulty_entry_1 > 0 OR ScriptName LIKE "%boss_%";
UPDATE `access_requirement` SET `quest_done_A` = 10285, `quest_done_H` = 10285, `quest_failed_text` = 'В данный момент доступ в подземелья закрыт.';
TRUNCATE TABLE `creature_ai_scripts`;
TRUNCATE TABLE `creature_ai_texts`;
TRUNCATE TABLE `creature_onkill_reputation`;
TRUNCATE TABLE `creature_text`;
TRUNCATE TABLE `db_script_string`;
TRUNCATE TABLE `gameobject_loot_template`;
TRUNCATE TABLE `disenchant_loot_template`;
TRUNCATE TABLE `fishing_loot_template`;
TRUNCATE TABLE `pickpocketing_loot_template`;
TRUNCATE TABLE `skinning_loot_template`;
TRUNCATE TABLE `prospecting_loot_template`;
TRUNCATE TABLE `creature_loot_template`;
DELETE FROM `event_scripts` WHERE `command` IN (0, 1, 7, 8);
DELETE FROM `waypoint_scripts` WHERE `command` IN (0, 1, 7, 8);
DELETE FROM `gameobject_scripts` WHERE `command` IN (0, 1, 7, 8);
DELETE `g`, `pg` FROM `gameobject` `g` LEFT JOIN `gameobject_template` `gt` ON `g`.`id` = `gt`.`entry` 
LEFT JOIN `pool_gameobject` `pg` ON `g`.`guid` = `pg`.`guid` WHERE `gt`.`type` IN (2, 3, 25);
DELETE FROM `gameobject_template` WHERE type IN (2, 3, 25);
UPDATE `creature_template` SET  `lootid` = 0, `pickpocketloot` = 0, `skinloot` = 0;
UPDATE `creature_transport` SET `emote` = 0;
UPDATE `item_template` SET `RequiredDisenchantSkill` = -1, `DisenchantID` = 0;
UPDATE `gameobject_template` SET `questItem1` = 0, `questItem2` = 0, `questItem3` = 0, `questItem4` = 0, `questItem5` = 0, `questItem6` = 0;
UPDATE `creature_template` SET `AIName` = '' WHERE `AIName` = 'EventAI' OR `AIName` = 'SmartAI';
TRUNCATE `quest_end_scripts`;
TRUNCATE `quest_poi`;
TRUNCATE `quest_poi_points`;
TRUNCATE `quest_start_scripts`;
TRUNCATE `gameobject_questrelation`;
TRUNCATE `gameobject_involvedrelation`;
TRUNCATE `pool_quest`;
TRUNCATE `game_event_seasonal_questrelation`;
TRUNCATE `lfg_dungeon_rewards`;
TRUNCATE `areatrigger_involvedrelation`;
DELETE FROM `event_scripts` WHERE `command` = 7;
DELETE FROM `disables` WHERE `sourceType` = 1;
DELETE FROM `spell_area` WHERE `quest_start` <> 0;
DELETE FROM `spell_area` WHERE `quest_end` <> 0;
DELETE FROM `smart_scripts` WHERE `event_type` IN (19, 20);
DELETE FROM `smart_scripts` WHERE `action_type` IN (6, 7, 15, 16, 26, 33);
DELETE FROM `smart_scripts` WHERE `action_type` = 53 AND `action_param4` <> 0;
DROP TABLE IF EXISTS `exploration_basexp`;
*/