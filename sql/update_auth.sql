﻿-- Update from ShadowHack3 wow server, autor Vitasic.


-- Core/Authserver: Added possibility to allow realm connections both from "world" and local networks. [6e80357f8e]
ALTER TABLE `realmlist`
ADD `localAddress` varchar(255) NOT NULL DEFAULT '127.0.0.1' AFTER `address`,
ADD `localSubnetMask` varchar(255) NOT NULL DEFAULT '255.255.255.0' AFTER `localAddress`;

-- Auth/Realmlist: Make use of RealmFlags and rename color to flag (core- and dbwise) [19f821d00a]
ALTER TABLE `realmlist` CHANGE `color` `flag` tinyint(3) unsigned NOT NULL DEFAULT '2';