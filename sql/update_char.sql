﻿-- Update from ShadowHack3 wow server, autor Vitasic.


-- fake_items
DROP TABLE IF EXISTS `fake_items`;
CREATE TABLE `fake_items` (
  `guid` int(11) NOT NULL,
  `fakeEntry` int(11) NOT NULL,
  PRIMARY KEY (`guid`)
);

-- Character db
CREATE TABLE IF NOT EXISTS `challenge_options` (
	`guid` BIGINT NOT NULL,
	`mode` TINYINT,
	`enable` TINYINT,
	PRIMARY KEY (`guid`)
);